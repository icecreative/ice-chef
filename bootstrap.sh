#!/bin/bash
sudo yum update -y
cd ~
curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -P chefdk -c stable -v 0.16.28
sudo rm -Rf /ssh
sudo mkdir /ssh
sudo chmod 777 /ssh
cd /ssh
sudo echo '-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAyjedRM+sCurDCV6V8aXtiNzlmiDNAVdFqbfmgyL6aw+w8pKa0NGdcIN0xd+g
cdKuf7E7EvdvsdpkS+L/UHscT289WOVbKtAUox1kpGYsotERr/6dcQTQX36PwpDAUuKk5Elxh8Y7
XtYeHqMUBjXK/5mT0R0O7hF+kkIntveMKGwDcq+9dSrNV5qHa/qta4vbp8+bKQPIgZrU+MqaY56r
ZqkVAKKJhCdlWHf6ow3XrV9khPZz7/7/vjsXFSjJ+DqrYHSH39g2uMCRSxJ0XaIy0vPv4mL8/6fO
VZ4dzYqnPKAgL1lrdYe0bYDaHf8c5SVi92l06RHTIrEBL1RdTruOIQIDAQABAoIBAH/tNvz1NC2j
3sbqo4lAI0lWFZqv7YFPqK55vMZxNmLMhnCu9mnsceFgCFu14Vagpev6Pxj8mBi3xIONk19iwsIt
6W13i3lGMggO7EBksxA9mybvuMJq/TesZV39HO5Sm2YgYEY4zg5s/pj7KgvTtByHF9BOxccuIhh8
6YsCiBR5QELUVN/eeqRtLBNVPTJEdTZw5pbK1ApAsnv01QwclCFyCukl3MDQeDWI7zFUfNOKufzR
gjv7ICAbKUEuV3DkZBWDcUIJaaP157NqlXEz9CgFvd+NG5lQVE7bnz3tYDluc9RwLl6cBnFsWSV4
WTWXLUAKLOfnUVjFF7BQb/D0/dECgYEA8WxBttEEF9+Yj/Mmg1QhBjlqp00NIqWsT9eAxMlwgKkX
lEQf/X/FH04KXwEWBGpEXseuVFv7pYT24BZyXdrOzYfjsPhVR1GO4Cmd2G5AKByi74yKSepqPzuL
vqhUwD5F8kLVO3wSHHiTg129CQUSLBe6Li/ZlZ19L5vjMH9zlscCgYEA1m1YRLUS3EbiLTZ6/+KA
vpeHbOJAudVhzuAv+AayDjOD16fIezr1xEmARohImcndBQz2U+e1OvweDBdwM1zMeynmn/2M/UzE
/vKQSnS/DEsDruvMIhzMdFu+2mGeiV5wB5+UmDWo1iN9woYocuDjQcubu+FgyZwVNuFqHBSHq9cC
gYEAuz7poO9wK0wCufMutcE0c+1oaSX7wBmbEbnlZ7uNBF0hqp9D6IuGygbvSExBVV9Oc+yPq2JE
RVZhW+BNB7pDl2mEEagqDEyXcgkVId4w6Gt9fCoWv8mOaki21EMeqJQkEAgB9kb+jRJ/5u13Rl2m
gIhOoLAAJGinxZs0lFLlxdUCgYEAjIQdWvEaSfW9lAlmszb4TW25VnYyGlWmAc7SQIQKvNmvZWxT
KcYqdhYAlLgMc5NgXkXvSm7GScAb5KqbYV1HttJvipq6gOeFFNlaCyDUbbbe1As7u4MJzJquyCc+
oCCd47EEzHYljSa26/nEmfubkWtfQ9De/19kW5F+WqZtfgMCgYBDF6QilgcyQOR1fumsBG8DcvC7
uEqcCnmHmkb2VR4XpDi3rxQHC6XyYnHk5RgxtsEARP2eGMci0HeJhvZA58/rsnGtEK5+h8Yx/YpO
Vsv5uSYbJO8um7LAKWInmMk69wXOaaT40aOCjf0wNa+N3tLPOfHQ4OSN4KuAsDlUafih/g==
-----END RSA PRIVATE KEY-----' > icekey_ireland.pem
sudo rm -Rf /chef
sudo mkdir /chef
cd /chef
sudo chmod 777 /chef
mkdir .chef
cd .chef
echo '
current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "lukeice"
client_key               "#{current_dir}/lukeice.pem"
chef_server_url          "https://api.chef.io/organizations/icecreative"
cookbook_path            ["#{current_dir}/../cookbooks"]' > knife.rb
echo '-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAv1dqrLHGsDsBjOD8usuTHllhqWIo7c7XJivYY0CJbldK3WUB
gR14pLGo0wqwFLMY/4F1DVObdeb1DWrfhOa4KTDenTmxdJZ4RBGuXGxzgVYlm4K2
O3lQH5pqhZUFlp7thE/hKq6jSgmEaZ4PcFyCqhpwJ+Xo4zfqJDe+pViQ7B8GsP6v
sSomaW475mR/eTPbFafninNQJXic0KhjH6NjiE7HBKIGP8OQT4jrvHCyrpWs5Rnk
UDE0IiMy3EvxtgtaHCVbL7iAUKc54qlqKgExRHKJJRv2GAl+D/R8eVPrUpEx56U9
EO4CeICh5shSVc16JtKMSbY+mJivPqyonoJG7wIDAQABAoIBAAjZayaXhj/qSrUQ
RsAA4En8CkdCMfBlq4+pYXWdq3197gl1PW+SRF0hllREX0ZlzhcrDpXSMxNt3hGD
ytijrwrAptsQqwNgXimPw8ekgGcP6+dYUUNI5grAIoMRFFQV/5gp9Zj6wLSQ4h3c
YIxqyuRBGsTNnkq78ElChr+w6+rvo94snvafmCHrNyfu/Sfe37RT0HEBAT4chKNZ
3k+mn5/35uZ87VOA8XPZzPfGZ651PC3yYSD0fIGTkKGrteIhn275diSdvJIiA+yx
/iedCIHtoCgGHe3iirUy9lj4m/EKedqMPE1egJLYMvSu+KPOb0z5nFbE2KNA6vPS
YbOrdtkCgYEA8DR2RLrVsQYODLr5VwZdlqUIiWvy69UzeeBZkKpzEgSNkRXJ0H+i
j/cfhaH6Drz5OH5ziAXGhmqJzXZOqOldN39SiaF4JQp/r2GzUy+mYbgsP0gHgAqm
IvIu84zT7+U5leHbmuQ8qpQoTlAubCPamgShD5osumhEzS5ewVjSMPMCgYEAy+xm
4Bahl3eIS/VEYhAD/xSvhObjhxn7bG4yIlBI9SY2XltOeHAfOztV/PxdLeEl/LR5
rKs9tOQammtzUD7UAqLUPpZ0SLQoNjyOIac6yjrtg2ktS+gESaejnrhkM7UZM7Y3
3gGMxtgIcZTrCdszkDMCKaD+i1wIBSMGvk3zcRUCgYEA4qat/hZJcrsi4LJAefQg
tPfOQVUhp5PJ+bNO7+lVoOe46PClOF5m9u8YyFBZkjSP+NWgj/XbKJQ4qS179XgV
pDs4l3NL2a+5YbSPr+ujVEujPKbrpM7nisv4r4jvpkol9x5hyBqaf95C1fpkg1el
enTpwL4afF3MpcqeUFROKS8CgYByiYN21f9JHgBV+ZrBIZG9EhdLpIujdHp9ep96
GnIAD8h10v8OMpu4Hym7ANwtSsqVV6EH1N7B3WT72uJX7hISsPZZeRQzSc1UOUrz
qAQjVlnVd5vTiKvv2tLR8WuGh29yViSm82Pow6pqcWRakt4xXhUW4LBYAgcXK4c8
GHhlyQKBgQDfrH6jH24j7hBqLXlmA0erWl6flIUGO407PhlKYuO4NTTlU8ZV+snh
+HQxKqEvF5IhSNCejXMOm0GsSqZccBxIXdTKHAQ+UyDzK86fc8Ao+e7COJYzsQrL
p/YkGWiq1EjgNK1K3+AN0zc0EacxCSx4HNBxRpUFGVLPSmMuC5+7hg==
-----END RSA PRIVATE KEY-----' > lukeice.pem
sudo knife client delete node-$(curl http://169.254.169.254/latest/meta-data/public-hostname) -y
sudo knife node delete node-$(curl http://169.254.169.254/latest/meta-data/public-hostname) -y
sudo knife bootstrap $(curl http://169.254.169.254/latest/meta-data/public-hostname) --ssh-user ec2-user --sudo --identity-file '/ssh/icekey_ireland.pem' --node-name node-$(curl http://169.254.169.254/latest/meta-data/public-hostname) --run-list 'recipe[apache-node]'
sudo umount /efs
sudo mkdir /efs
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).fs-3bdf22f2.efs.eu-west-1.amazonaws.com:/ /efs
cd ~
crontab -l > mycron
echo "* * * * * sudo chef-client" >> mycron
crontab mycron
rm mycron