name 'apache-node'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures apache-node'
long_description 'Installs/Configures apache-node'
version '0.24.0'
depends 'httpd', '~> 0.4'
depends 'php-fpm', '~> 0.7'
# If you upload to Supermarket you should set this so your cookbook
# gets a `View Issues` link
# issues_url 'https://github.com/<insert_org_here>/apache-node/issues' if respond_to?(:issues_url)

# If you upload to Supermarket you should set this so your cookbook
# gets a `View Source` link
# source_url 'https://github.com/<insert_org_here>/apache-node' if respond_to?(:source_url)
