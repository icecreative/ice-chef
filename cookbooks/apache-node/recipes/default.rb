package 'tree'
package 'htop'
package 'git'

httpd_service 'apache' do
	mpm 'prefork'
	action [:create, :start]
end

httpd_config 'apache' do
	instance 'apache'
	source 'httpd.conf.erb'
	notifies :restart, 'httpd_service[apache]'
end

httpd_module 'php' do
  instance 'apache'
end

package 'php-mysql' do
  action :install
  notifies :restart, 'httpd_service[apache]'
end